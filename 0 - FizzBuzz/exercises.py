# Gebe Hello World in die Konsole aus



# Gebe den Wert der Variable my_variable aus 
my_variable = 'My Hello World'



# Gebe my_variable den Zahlenwert 42



# Gebe den Wert der Variable my_variable aus



# Oft wollen wir etwas ausführen, wenn ein Bedingung erfüllt ist.
# Dafür können wir eine if-Anweisung verwenden.
# Die Syntax ist:
bedingung = True
if bedingung:
    # Dieser Teil ist eingerückt. Dadurch weiß Python,
    # dass dieser Teil nur ausgeführt wird, wenn die
    # Bedingung erfüllt ist.
    print('Bedingung erfüllt') 
# Dieser Teil ist nicht eingerückt. Dadurch weiß Python,
# dass dieser Teil immer ausgeführt wird.
print('Ich werden immer ausgeführt')

# Überprüfe, ob die Variable my_variable größer als 30 ist.
# Wenn ja, gebe "my_variable ist größer als 30" aus.



# Wenn die Bedingung nicht erfüllt ist, können wir mit einer
# elif-Anweisung eine oder mehrere weitere Bedingungen überprüfen.
# Mit der else-Anweisung können wir eine Anweisung ausführen,
# wenn alle anderen Bedingungen nicht erfüllt werden.
bedingung = False
if bedingung:
    print('Bedingung erfüllt')
elif my_variable == 3:
    print('my_variable ist 3')
elif my_variable == 4:
    print('my_variable ist 4')
else:
    print('Bedingungen nicht erfüllt')

# Überprüfe, ob die Variable my_variable größer als 30 ist.
# Wenn ja, gebe "my_variable ist größer als 30" aus. Wenn nein,
# gebe "my_variable ist nicht größer als 30" aus.


# Viele Aufgaben wollen wir mehrmals ausführen.
# Dafür können wir eine for-Schleife verwenden.
# Die Syntax ist:
for i in range(10):
    # Dieser Teil wird 10 mal ausgeführt.
    print(i)
    # Die Variable i nimmt nacheinander die Werte 0, 1, 2, 3, 4, 5, 6, 7, 8, 9. an.

# Gebe die Zahlen von 0 bis 99 aus.



# Schreibe ein Programm, das FizzBuzz von 1 bis 100 spielt.

# Nochmal zur Erinnerung:
# - Vielfache von 3 werden zu Fizz
# - Vielfache von 5 werden zu Buzz
# - Vielfache von 3 und 5 werden zu FizzBuzz



# Zusatzaufgabe
# Mit der Funktion `input("Bitte Eingabe machen: ")` kannst du eine 
# Eingabe über die Konsole machen. Schreibe FizzBuzz so um, dass du das Spiel spielst, und der Computer überprüft, ob du alles richtig machst.

# __Vorsicht:__ `input()` gibt immer einen `str` zurück. Du musst die
# Zahlen, die du mit dem input vergleichst, in einen `str` umwandeln.
# Beispiel: `str(2)`
