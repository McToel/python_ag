# FizzBuzz
for i in range(1, 101):
    if i % 15 == 0:
        print('FizzBuzz')
    elif i % 3 == 0:
        print('Fizz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)


# Zusatzaufgabe
for i in range(1, 101):
    richtiges_ergebnis = i
    if i % 15 == 0:
        richtiges_ergebnis = "FizzBuzz"
    elif i % 3 == 0:
        richtiges_ergebnis = "Fizz"
    elif i % 5 == 0:
        richtiges_ergebnis = "Buzz"

    if not input("Eingabe: ") == str(richtiges_ergebnis):
        print("FALSCH!! Du hast verloren")
        break
else:
    print("Gewonnen! Herzlichen Glückwunsch!")