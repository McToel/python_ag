
Heute wollen wir unsere Programmier-Skills verwenden um Verschlüsselungen zu knacken. Wie das beim caesar-chiffre geht findet ihr hier raus: https://de.wikipedia.org/wiki/Caesar-Verschl%C3%BCsselung

Aber so kompliziert brauchen wir es vielleicht gar nicht. Für kurze Sätze reicht es oft, einfach alle 26 Möglichkeiten durchzuprobieren. So was findet ihr auch hier mal als Beispiel: https://www.dcode.fr/caesar-cipher

TIPP: Um z.B.: herauszufinden, wie man die häufigkeit von Buchstaben in einem Text zählt, lohnt es sich eigentlich immer, einfach mal Google zu fragen.

Den Code für die Ceasar-Verschlüsselung gibt es übrigens auch nochmal hier: https://gitlab.com/McToel/python_ag/-/blob/main/7/caesar_cipher.py