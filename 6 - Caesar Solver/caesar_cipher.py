def character_to_number(char):
    return ord(char) - 65


def number_to_character(number):
    return chr(number + 65)


def shift_character(char, shift):
    return number_to_character((character_to_number(char) + shift) % 26)


def caesar_cipher(text, shift):
    result = ""
    for char in text:
        # Ist das Zeichen alphanumerisch? (A-Z, a-z, 0-9)
        if char.isalpha():
            result += shift_character(char, shift)
        else:
            result += char
    return result


text = input('Enter text: ').upper()
shift = int(input('Enter shift: '))

# Verschlüsselung: verschiebe jedes Zeichen um shift
encrypted = caesar_cipher(text, shift)
print("encrypted:", encrypted)

# Entschlüsselung: verschiebe jedes Zeichen um -shift
decrypted = caesar_cipher(encrypted, -shift)
print("decrypted:", decrypted)