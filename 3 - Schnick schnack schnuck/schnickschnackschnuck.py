import random

schere = "Schere"
stein = "Stein"
papier = "Papier"

gesten = [schere, stein, papier]

gewinnt = {
    schere: papier,
    stein: schere,
    papier: stein
}

def gewonnen(geste_spieler, geste_computer, gewinnt):
    if geste_spieler == geste_computer:
        return 'Unentschieden'
    elif gewinnt[geste_spieler] == geste_computer:
        return 'Gewonnen'
    else:
        return 'Verloren'


while True:
    computer_geste = random.choice(gesten)
    geste_spieler = input('Was für eine Geste möchtest du machen? ')
    if geste_spieler not in gesten:
        print('OHNE BRUNNEN!!!!')
        continue
    print(gewonnen(geste_spieler, computer_geste, gewinnt))