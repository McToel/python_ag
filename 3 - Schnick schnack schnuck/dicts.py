# Ein dict (dictionary -> Wörterbuch) erstellen
person = {
    "name": "John",
    "age": 30,
    "country": "Norway",
    "city": "Oslo",
}

# Den Wert eines keys (Schlüssels) ausgeben
print(person["name"])

# Den Wert eines keys (Schlüssels) ändern
person['age'] = 31
print(person['age'])

# Einen neuen key mit einem neuen value (Wert) hinzufügen
person['job'] = 'programmer'
print(person["job"])

# Überprüfen, ob ein key in einem dict vorhanden ist
if "job" in person:
    print("Der key 'job' ist in dem dict vorhanden")

# Iteriere (for-Schleife) über alle keys in einem dict
for key in person:
    print(key)


# Aufgagen:
# Erstelle ein dict, in dem gespeichert ist wie du heißt,
# wie alt du bist und auf welche Schule du gehst.
...


# Stelle dir vor du wechselst zu einer anderen Schule. Ändere
# die Schule zu der, auf der du bist.
...


# Nun nehmen wir mal an, du bist mit der Schule fertig und willst
# eine Ausbildung machen. Füge einen neuen key (Schlüssel) mit
# deinem zukünftigem Ausbildungsort hinzu.
...


# Hast du auch schon vergessen was du alles gespeichert hast?
# Mache ein for Schleife und gebe alle keys und values aus.
...