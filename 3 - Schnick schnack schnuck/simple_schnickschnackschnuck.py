import random

gewinnt = {
    'Schere': 'Papier',
    'Stein': 'Schere',
    'Papier': 'Stein'
}

computer_geste = random.choice(list(gewinnt))

mensch_geste = input('Was für eine Geste möchtest du machen? ')

print('Computer vs. Mensch')
print(computer_geste, 'vs.', mensch_geste)

if computer_geste == mensch_geste:
    print('Unentschieden')
elif gewinnt[computer_geste] == mensch_geste:
    print('Gewonnen')
else:
    print('Verloren')