import pandas as pd
import matplotlib.pyplot as plt
# in serverDeploy.txt haben wir die Stellendaten der
# Stadt Tübingen gespeichert. Diese wollen wir jetzt
# mal visualisieren.
# Um mit den Daten zu arbeiten verwenden wir pandas
# (https://pandas.pydata.org/). Als erstes müssen wir
# die Daten dafür einlesen. Das format jetzt ist nicht
# zum einlesen geeignet. Konvertiere die Datei in eine
# .csv (Comma Seperated Values). Nenne die Spalten
# date, intern und extern


# Lese die .csv Datei ein. Dafür kannst du
# pandas.read_csv (https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html?highlight=read_csv#pandas-read-csv).
# Die meisten Parameter kannst du dabei einfach ignorieren.
stellen = ...


# Die Spalte date soll ein Datum enthalten. Wenn
# die csv Datei eingelesen wird, wird dies aber nicht
# automatisch erkann. Konvertiere die Spalte date in
# ein richtiges Datetime-Format. hierfür kannst du
# pd.to_datetime(spalte, format=...) (https://pandas.pydata.org/docs/reference/api/pandas.to_datetime.html?highlight=to_datetime#pandas-to-datetime)
# verwenden.
# Hint: Datumsformat: '%d.%m.%Y  %H:%M:%S'
stellen['date'] = ...

# Setze die Spalte date als Index vom Dataframe. Dafür
# kannst du set_index verwenden (https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.set_index.html?highlight=set_index#pandas-dataframe-set-index)


# Plotte die Daten. .plot() wird dir weiter helfen (https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.plot.html?highlight=plot#pandas.DataFrame.plot)


# Vergesse nicht plt.show() aufzurufen um den Plot anzuzeigen
plt.show()