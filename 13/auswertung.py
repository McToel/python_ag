import pandas as pd
import matplotlib.pyplot as plt

stellen = pd.read_csv('serverDeploy.csv')
stellen['date'] = pd.to_datetime(stellen['date'], format='%d.%m.%Y  %H:%M:%S')
stellen.set_index('date', inplace=True)

stellen['dif'] = stellen['extern'] - stellen['intern']
plot = stellen.plot(kind='line')

plt.scatter(x=stellen.index, y=stellen['extern'])
plt.scatter(x=stellen.index, y=stellen['intern'])
plt.scatter(x=stellen.index, y=stellen['dif'])

plt.show()