def character_to_number(char):
    return ord(char) - 65


def number_to_character(number):
    return chr(number + 65)


def shift_character(char, shift):
    return number_to_character((character_to_number(char) + shift) % 26)


def caesar_cipher(text, shift):
    for char in text:
        if char.isalpha():
            print(shift_character(char, shift), end="")
        else:
            print(char, end="")


text = input("Text: ")
shift = int(input("Shift: "))
caesar_cipher(text, shift)