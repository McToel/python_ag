# Caesar Cipher

## Aufgaben
Schaue dir die Infos hier zum Caesar Chiffre an: https://de.wikipedia.org/wiki/Caesar-Verschl%C3%BCsselung
Hier nochmal eine kurze Hilfe um Buchstaben in die entsprechenden ASCII Codes umzuwandeln:
```py
# Buchstabe -> ASCII Code
ord(buchstabe)

# ASCII Code -> Buchstabe
chr(ascii_code)
```

Wir wollen standartmäßig keine Leer- und Satzeichen verschlüsseln. Bevor wir ein Zeichen verschlüsseln, sollten wir daher überprüfen, ob es "alphanumeric" ist. Alphanumeric heist, dass es entweder im **Alpha**bet vorkommt oder **numerisch**, also eine Zahl ist.
Dafür kannst du `.isalpha()`

- Mache eine Cäsar Verschlüsselung
- Mache eine Cäsar Entschlüsselung
- Hilf dir dabei, den einen unbekannten Schlüssel zu finden

## Zusatz
- Baue einn Vigenère-Chiffre


## Tipps
```py
def character_to_number(char):
    return ord(char) - 65


def number_to_character(number):
    return chr(number + 65)
```