# run pip install requests

import html
import requests

# generate random german word
def generate_word():
    response = requests.get('https://alex-riedel.de/randV2.php?anz=1')
    return html.unescape(response.json()[0]).lower()

# print(generate_word())