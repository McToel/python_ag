from wortgenerator import generate_word

word = generate_word()
not_guessed = list('abcdefghijklmnopqrstuvwxyzäöüß')
tries = 10
# Mache eine Variable, in der du das zu eratende Wort speicherst
# Mache eine Liste (namens not_guessed) mit allen Buchstaben, die möglich sind
# Mache eine Variable, in der gespeichert wird, wie viele Fehlversuche noch erlaubt sind


def format_word(word: str, not_guessed: list):
    """Ersetze alle Buchstaben von word, die noch nicht erraten wurden,
    mit einem '-'

    Parameters
    ----------
    word : str
        Richtige Lösung
    not_guessed : list
        Liste mit Buchstaben, die noch nicht erraten wurden

    Returns
    -------
    str
        word, aber ohne den unbekannten Buchstaben
    """
    for c in word:
        if c in not_guessed:
            word = word.replace(c, '-')
    return word

print(format_word(word, not_guessed))

# Solange noch tries übrig sind, soll der User einen Buchstaben eingeben können.
# Wenn der Buchstabe in not_guessed ist, dann entferne ihn daraus, wenn er nicht darin ist,
# dann sage dem User, dass dieser Buchstabe schon erfragt wurde.
# Wenn der Buchstabe nicht im Wort ist, dann Ziehe 1 von den noch erlaubten Fehlversuchen ab.
# Gebe das Wort aus, aber ohne den noch nicht erratenen Buchstaben. Verwende dafür die
# Funktion format_word()
# Wenn in dem Fomatierten Wort keine '-' mehr sind, hat der User gewonnen.
while tries:
    c = input('Enter Charakter: ')
    if c in not_guessed:
        not_guessed.remove(c)
        if c not in word:
            print(' \u0336' + c)
            tries -= 1
            print('Tries left:', tries)
    else:
        print('You have already guessed this')

    word_to_print = format_word(word, not_guessed)
    print(word_to_print)
    if '-' not in word_to_print:
        print('You have won!')
        break

print('LOOOSER!!! YOU HANGED A MAN')
print('Das wort war:', word)