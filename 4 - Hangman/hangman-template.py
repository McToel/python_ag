# Hier habe ich einfach mal ein Wort eingegeben.
# Später kannst du hier zufällig ein Wort generieren.
word = 'geheimwort'
fehlversuche = 0
guessed = []


def format_word(word, guessed):
    for buchstabe in word:
        if buchstabe in guessed:
            pass
        else:
            word = word.replace(buchstabe, '-')
    return word


while fehlversuche < 10:
    print(format_word(word, guessed))
    buchstabe = input('Buchstabe eingeben').lower()
    guessed.append(buchstabe)



# Zusatz:
# Füge Ascii Art für die verschiedenen Stufen hinzu
# Generiere irgendwie zufällige Wörter