# Mit def wird eine Funktion definiert.
# Eine Funktion ist quasi ein kleines Subprogramm.
# Das der Inhalt der Funktion wird dann ausgeführt, wenn
# die Funktion mit dem Namen aufgerufen wird.
def meine_funktion():
    print('Meine Funktion wird ausgeführt')

# Rufe die Funktion meine_funktion() auf. Wichtig sind
# die Klammern. Diese sorgen erst dafür, dass die Funktion
# ausgeführt wird. Ohen diese passiert nichts.
meine_funktion()


# Wenn eine Funktion ein return statement hat, liefert sie
# einen Rückgabewert zurück. In diesem Fall gibt die Funktion
# den Wert der Zahl pi zurück.
def pi():
    return 3.141592653589793

rückgabewert = pi()
print(rückgabewert)


# In den meisten Fällen soll eine Funktion nicht immer genau
# das gleiche machen und auch nicht immer den selben Rückgabewert
# liefern. Eine Funktion wie pi() ist total unnötig. Stattdessen
# könnte man auch einfach eine Variable verwenden:
PI = 3.141592653589793

# Deshlab kann eine Funktion auch Argumente annehmen, mit denen
# dann z.B. gerechnet werden kann
def addiere(a, b):
    return a + b

ergebnis = addiere(1, 2)
print(ergebnis)


# Aufgaben:
# Erstelle eine Funktion, die zwei Zahlen subtrahiert und das Ergebnis
# zurückgibt.
...

# Erstelle eine Funktion, die zählt wie oft ein Buchstabe in einem Wort
# vorkommt.
# Tipp:
# for buchstabe in "Hallo":
#     ...
# gibt ein Buchstabe für ein Buchstabe.
...

# Erstelle eine Funktion, die zwei Wörter als Argumente nimmt und alle
# Buchstaben die in beiden Wörtern vorkommen zurückgibt.
...

# Erstelle eine Funktion, die zwei Wörter als Argumente nimmt. Alle
# Buchstaben aus dem ersten Wort, die auch im zweiten Wort vorkommen,
# sollen im zweiten Wort mit einem - ersetzt werden.
# Tipp:
# 'Hallo'.replace('l', '-')
# ersetzt alle l in 'Hallo' durch ein -.

# Beispiel:
# 'abc' und 'hallo berlin'
# -> 'h-llo -erlin
