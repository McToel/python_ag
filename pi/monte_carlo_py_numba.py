import random
from numba import jit, prange

@jit(nopython=True, parallel=False)
def calc_pi(N_SAMPLES: int) -> float:
    n_in_circle = 0
    for _ in prange(N_SAMPLES):
        x, y = random.random(), random.random()
        if x ** 2 + y ** 2 <= 1:
            n_in_circle += 1

    pi = 4 * (n_in_circle / N_SAMPLES)
    return pi

N_SAMPLES = 1_000_000_000
pi = calc_pi(N_SAMPLES)

print(pi)