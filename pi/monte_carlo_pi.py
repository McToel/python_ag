import numpy as np
import random

N_SAMPLES = 100_000_000

positions = np.random.rand(N_SAMPLES, 2) * 2 - 1
in_circle = (positions[:, 0] ** 2 + positions[:, 1] ** 2) <= 1
n_in_circle = in_circle.sum()

pi = 4 * (n_in_circle / N_SAMPLES)
print('Pi:', pi)