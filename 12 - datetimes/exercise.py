from datetime import datetime, timedelta

# Docs: https://docs.python.org/3/library/datetime.html#module-datetime

# Gibt die aktuelle Zeit und das aktuelle Datum in die Konsole aus


# Gibt das aktuelle Datum in die Konsole aus


# Gibt die aktuelle Zeit in die Konsole aus


# Für die nächsten Aufgaben wirst du nicht nur datetime
# brauchen, sondern auch timedelta. Finde mithilfe des Internets
# heraus, wie du diese Aufgaben lösen kannst. Es lohnt sich immer auf
# Englisch zu suchen!
# Gibt das Datum von vorgestern in die Konsole aus


# Gibt das Datum von vor 10, 100 und 42 Tagen aus


# Wie viele Minuten liegen folgende Daten auseinander:
# 23:54 12.01.2022
# 09:48 14.01.2022