from datetime import datetime

now = datetime.now()
print(now)
print(now.year)
print(now.month)
print(now.day)
print(now.hour)
print(now.minute)
print(now.second)

# Formatierung mit strftime (string-format-time)
# strftime benötigt einen String, der das Format bestimmt.
# Ein Beispiel wäre diese Formatierung: '%d.%m.%Y' -> z.B. '14.10.2022'
print(now.strftime('%d.%m.%Y'))
# In diesem Fall wird das %d mit dem Tag im Monat ersetzt,
# das %m mit dem Monat und das %Y mit dem Jahr. Die Punkte
# bleiben einfach so wie sie waren.
# Hier gibt es eine Liste mit allen Formatierungscodes:
# https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes


# Andersherum geht das auch, mit strptime (string-parse-time)
# Wir müssen hier wieder das Format angeben
some_date = datetime.strptime('14.10.2022', '%d.%m.%Y')


# Wir können auch direkt ein Datum angeben
another_date = datetime(2022, 10, 14)
# Da das die Englische Schreibweise (2022-10-14) ist, kann
# ist das auch der standard für das Datetime Modul