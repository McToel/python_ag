# Hier hast du ein paar listen.
age = [13, 16, 11, 19, 21]
name = ['Alice', 'Bob', 'Carol', 'Dave', 'Eve']
gender = ['♀', '♂', '⚧', '♂', '♀']
# Erstelle aus diesen Listen ein dictionary, das wie folgt aussieht
{
    'Alice': {
        'age': ...,
        'gender': ...
    },
    ...: ...
}