# Übersicht:
# https://docs.python.org/3/library/functions.html

# Print: Ausgabe in die Konsole
# https://docs.python.org/3/library/functions.html#print
print("Hello World!")

# Len: Länge eines Strings, List, Tuple, Dictionary
# https://docs.python.org/3/library/functions.html#len
print(len("Hello World"))
print(len([1, 2, 3, 4, 5]))

# Range: Liste von Zahlen
# https://docs.python.org/3/library/stdtypes.html#typesseq-range
print(list(range(10)))
for i in range(10):
    print(i)

# Input: Eingabe durch die Konsole
# https://docs.python.org/3/library/functions.html#input
eingabe = input("Gebe einen Text ein: ")
print(eingabe)

# Enumerate: Iteriere über eine Liste und liefere die Indexe und Werte zurück
# https://docs.python.org/3/library/functions.html#enumerate
for index, value in enumerate(['tic', 'tac', 'toe']):
    print(index, value)

# reversed: Iteriere über eine Liste in umgekehrter Reihenfolge
# https://docs.python.org/3/library/functions.html#reversed
for value in reversed(['tic', 'tac', 'toe']):
    print(value)

# sorted: Sortiere eine Liste
# https://docs.python.org/3/library/functions.html#sorted
print(sorted(['tic', 'tac', 'toe']))
print(sorted([40, 3, 32, 7, 1, 9, 17, 2, 5, 6]))

# min: Liefere den kleinsten Wert in einer Liste
# https://docs.python.org/3/library/functions.html#min
print(min([40, 3, 32, 7, 1, 9, 17, 2, 5, 6]))

# max: Liefere den größten Wert in einer Liste
# https://docs.python.org/3/library/functions.html#max
print(max([40, 3, 32, 7, 1, 9, 17, 2, 5, 6]))

# sum: Liefere die Summe aller Werte in einer Liste
# https://docs.python.org/3/library/functions.html#sum
print(sum([40, 3, 32, 7, 1, 9, 17, 2, 5, 6]))

# round: Runde eine Zahl auf eine bestimmte Anzahl von Nachkommastellen
# https://docs.python.org/3/library/functions.html#round
print(round(3.141592653589793, 2))

# abs: Liefere den Betrag einer Zahl
# https://docs.python.org/3/library/functions.html#abs
print(abs(-3.141592653589793))

# int: Liefere einen Integer-Wert
# https://docs.python.org/3/library/functions.html#int
print(int(3.141592653589793))
print(int('10'))

# float: Liefere einen Float-Wert
# https://docs.python.org/3/library/functions.html#float
print(float("3.141592653589793"))

# type: Gibt den Typ eines Objekts zurück
# https://docs.python.org/3/library/functions.html#type
print(type(3.141))
print(type("3.141"))
print(type(3))

# isinstance: Prüfe, ob ein Objekt eine bestimmte Klasse / Typ ist
# https://docs.python.org/3/library/functions.html#isinstance
print(isinstance(3.141, float))
print(isinstance("3.141", str))
print(isinstance('10', int))

# str: Liefere einen String-Wert
# https://docs.python.org/3/library/functions.html#str
print(str(42))
print(isinstance(str(42), str))

# map: führe eine Funktion auf jedes Element einer Liste aus
# https://docs.python.org/3/library/functions.html#map
def double(x):
    return x * 2

# Ohne map:
for i in range(10):
    print(double(i))

# Mit map:
doubled = map(double, range(10))
print(list(doubled))

# filter: liefere eine Liste mit den Elementen, die eine bestimmte Funktion erfüllen
# https://docs.python.org/3/library/functions.html#filter
def is_even(x):
    return x % 2 == 0

# Ohne filter:
for i in range(10):
    if is_even(i):
        print(i)

# Mit filter:
evens = filter(is_even, range(10))
print(list(evens))

# zip: kombiniere zwei Listen in einer Liste
# https://docs.python.org/3/library/functions.html#zip
print(list(zip(range(10), range(10, 20))))

for a, b in zip(['a', 'b', 'c'], [1, 2, 3]):
    print(a, b)
    