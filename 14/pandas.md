# Pandas
## Install
Folgendermaßen kannst du pandas installieren

```pip install pandas```

## Docs
[Pandas Dokumentation: https://pandas.pydata.org/docs/index.html](https://pandas.pydata.org/docs/index.html)

[10 minutes to pandas](https://pandas.pydata.org/pandas-docs/stable/user_guide/10min.html)

# Aufgaben
Lese den Iris-Datensatz ein. Dafür kannst du folgende Befehl verwenden

`iris = pd.read_csv('https://gitlab.com/McToel/python_ag/-/raw/main/14/iris.csv')`

Danach sollst du einige Dinge mit dem Datensatz machen. Hierfür ist sowohl die pandas Dokumentation als auch Google sehr hilfreich. Meistens finden man die Ergebnisse am besten auf Englisch. Wenn man da mal was nicht versteht, hilft z.B. Deepl translator.

Wenn du mehr über den Iris Datensatz erfahren willst, kannst du einfach "iris dataset" googeln.

1. Wie viele Datenpunkte (rows) hat der Datensatz
2. Lasse dir den Kopf (head) des Datensatzes ausgeben
3. Was für Spalten (columns) hat der Datensatz
4. Was sind die verschiedenen Iris "Klassen"
5. Gib alle Datenpunkte aus, bei denen die `petalwidth` größer als 2 ist
6. Wie groß ist die `petalwidth` durchschnittlich
7. Wie groß ist die `petalwidth` durchschnittlich innerhalb der verschiedenen Iris Klassen
8. Erstelle einen Plot, in dem zu sehen ist, wie sich die Iris Klassen unterscheiden


