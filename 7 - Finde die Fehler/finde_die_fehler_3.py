x = int('Enter value for x:')
print(x)



# Primzahlen
def is_prime(x):
    for i in range[x]:
        if x % i == 0:
            return False
    return True

for i in range[2, 100]:
    if is_prime(i):
        print(i, end='\r')



# progress spinner
import time
delay_in_seconds = 0.5
for _ in range(10):
    print('|', end='\r')
    time.sleep(delay_in_seconds)
    print('/', end='\r')
    time.sleep(delay_in_seconds)
    print('-', end='\r')
    time.sleep(delay_in_seconds)
    print('\\', end='\r')
