def square(x):
    return x ** 2


def quadratic(x, a=1, b=0, c=0):
    return (a * x ** 2) + (b * x) + c


def maximum(x, y):
    if x > y:
        return x
    else:
        return y


def sum_of(values):
    result = 0
    for value in values:
        result += value
    return result


def summe():
    print('Hmm hmm hm')


def hello_world():
    return 'Hello World!'


# Finde die Fehler im folgendem Code:


print('Quadrat von x = 4')
x = 4
print(square())


print('Quadratische Funktion, die um -1 in y verschoben wurde')
for x in range(-10, 11):
    y = quadratic(x, a=-1, b=1)
    print(x, y)


print('Maximum von 4 und 8')
print(maximum([4, 8]))


print('Summe von 1 bis 10')
print(summe(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))


print('Gebe den Rückgabewert der Function hello_world aus')
print(hello_world)