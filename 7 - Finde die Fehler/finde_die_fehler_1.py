random_list = ['foo', 'bar', 'baz', 'qux', 'quux']
first_item = random_list['0']
last_item = random_list[len(random_list)]


random_dict = {'foo': 'bar', 'baz': 'qux', 'quux': 'quuz'}
first_item = random_dict[0]
total_items = len(random_dict) - 1


some_number = 0
while some_number < 10:
    some_number += 1
    print(some_number)


number = '4.5'
if number == 4.5:
    print('Number is 4.5')


