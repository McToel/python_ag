
s = """
{l[1]}|{l[2]}|{l[3]}
-----
{l[4]}|{l[5]}|{l[6]}
-----
{l[7]}|{l[8]}|{l[9]}
"""

spielfeld = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']

feld = int(input('Wo willst du dein Kreuz setzen? '))
spielfeld[feld] = 'x'
print(s.format(l=spielfeld))







### Verzweigungen
a = 42
if a == 42:
    print('a is 42')
elif a == 43:
    print('a is not 42 but 43')
elif a % 2 == 0:
    print('a is even')
else:
    print('a is not 42 and not 43')
### Schleife (loop)
for i in range(10):
    print(i) # Ausgabe: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9