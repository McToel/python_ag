def check_winner(board):
    # Horizontalen (Zeilen) überprüfen
    for i in range(1,10,3):
        if board[i] == board[i+1] == board[i+2] != ' ':
            return board[i]
        
    # Vertikalen (Spalten) überprüfen
    for i in range(1,4):
        if board[i] == board[i+3] == board[i+6] != ' ':
            return board[i]
    
    # Diagonalen überprüfen
    if board[1] == board[5] == board[9] != ' ':
        return board[1]
    
    elif board[7] == board[5] == board[3] != ' ':
        return board[7]
    
    else:
        return False


board = """
┌─1─┬─2─┬─3─┐
│ {c[1]} │ {c[2]} │ {c[3]} │
├─4─┼─5─┼─6─┤
│ {c[4]} │ {c[5]} │ {c[6]} │
├─7─┼─8─┼─9─┤
│ {c[7]} │ {c[8]} │ {c[9]} │
└───┴───┴───┘
"""
states = [' ' for _ in range(10)]
print(board.format(c=states))

for i in range(9):
    player = 'x' if i % 2 else 'o'
    while True:
        field = int(input('Player ' + player + ': '))
        if states[field] == ' ':
            states[field] = player
            break
        else:
            print('Please select an empty field')

    print(board.format(c=states))

    winner = check_winner(states)
    if winner:
        print('And the winner is:', winner)
        break
