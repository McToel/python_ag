my_str = 'Das hier ist ein string'
my_str2 = "Das hier auch. Eigentlich ist es egal, welche Anführungszeichen ich verwende"

print('Content of my_str')
print(my_str)

my_input = input('Bitte gebe etwas ein: ')
print('Du hast folgendes eingegeben:')
print(my_input)

concatted = 'Du kannst ' + 'auch strings ' + 'aneinanderhängen (concat)'
print(concatted)

my_zahl_str = '42'
my_zahl_int = int(my_zahl_str)

multiline = """
Ich bin ein Multiline
String
"""

print('mich kann man {a} formatieren'.format(a=42))