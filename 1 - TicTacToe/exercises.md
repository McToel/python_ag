## Aufgabe 1
Entwerfe ein Spielfeld, auf dem TikTakToe gespielt werden kann, und gebe dieses in die Konsole aus.
Vielleicht helfen dir ja diese Zeichen: ┌ ─ ┬ ┐ │ ├ ┼ ┤ └ ┴ ┘

## Aufgabe 2
Die Spieler*innen sollen abwechselnd eingeben, auf welches Feld sie ein `x` oder ein `o` setzen wollen.

__Vorsicht:__ `input()` gibt immer einen `str` zurück. Du muss den Input also noch in ein int umwandeln: `int('7')`


## Aufgabe 3
Lasse die Spieler*innen die Kreuze nur dahin setzen, wo noch Platz ist.

## Zusatzaufgabe
Teste automatisch nach jeder Eingabe, ob das Spiel gewonnen wurde und gratuliere ggf. dem/der Gewinner*in

