board = """
┌─0─┬─1─┬─2─┐
│ {c[0]} │ {c[1]} │ {c[2]} │
├─3─┼─4─┼─5─┤
│ {c[3]} │ {c[4]} │ {c[5]} │
├─6─┼─7─┼─8─┤
│ {c[6]} │ {c[7]} │ {c[8]} │
└───┴───┴───┘
"""
states = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
print(board.format(c=states))

for i in range(9):
    field = int(input('Bitte Feld eingeben: '))
    if i % 2 == 0:
        states[field] = 'o'
    else:
        states[field] = 'x'

    print(board.format(c=states))
