### Ausgabe in die Konsole
print('Das, was ausgeben werden soll')

### Variablen
a = 42 # Ganzzahl (int)
b = 1.5 # Kommazahl (float)
c = 'Das ist eine Zeichenkette' # Zeichenkette (str)
'1, 2, {a}, 4, {b}'.format(a=3, b=5) # >>> '1, 2, 3, 4, 5'
s = """
This is a
multiline
str
"""

### Listen
l = ['foo', 'bar', 1, 42]
len(l) # >>> 4
l[0] # >>> 'foo'
l[2] # >>> 1

### Schleife (loop)
for i in range(10):
    print(i) # Ausgabe: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9

while a >= 42:
    print('this stops if a gets smaller than 42')
    if a == 43: # if a is 43, stop the loop immediately
        break

### Funktion
def function2(parameter1, parameter2):
    return parameter1 + parameter2

### Verzweigungen
a = 42
if a == 42:
    print('a is 42')
elif a == 43:
    print('a is not 42 but 43')
elif a % 2 == 0:
    print('a is even')
else:
    print('a is not 42 and not 43')

### Arithmetik
print('Addition:', 1 + 2) # Ausgabe: 3
print('Subtraktion:', 2 - 1) # Ausgabe: 1
print('Multiplikation:', 5 * 3) # Ausgabe: 15
print('Potenz:', 4 ** 2) # Ausgabe: 16
print('Division:', 8 / 4) # Ausgabe: 2.0
print('Floor Division:', 7 // 2) # Ausgabe: 3
print('Modulo (Rest):', 7 % 2) # Ausgabe: 1
