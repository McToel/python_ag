squares = []
for i in range(10):
    squares.append(i**2)

print(squares)


squares2 = [i**2 for i in range(10)]

print(squares2)


equal_squares = [i**2 for i in range(10) if i % 2 == 0]

print(equal_squares)


alphabet = 'abcdefghijklmnopqrstuvwxyz'

letter_numbers = {letter: i for i, letter in enumerate(alphabet)}

print(letter_numbers)
