# List comprehension ist eine eine Art for-Loop, die eine Liste erzeugt.
# Folgendes Beispiel:

# Nehmen wir zum Beispiel an, wir wollen eine Liste von Quadraten erstellen, etwa so:
squares = []
for x in range(10):
    squares.append(x ** 2)



# Mit einer List comprehension können wir das gleiche auch schreiben:
squares = [x ** 2 for x in range(10)]
print(squares)


# Oder sagen wir, wir wollen Liste aus den Buchstaben des Wortes "Hello":
letters = [letter for letter in 'Hello']
print(letters)


# Die List comprehension kann mithilfe von if-Bedingungen auch elemente auslassen:
# Wir wollen eine Liste von Quadraten erstellen, die nicht durch 3 teilbar sind:
squares = [x ** 2 for x in range(10) if x % 3 != 0]
print(squares)


vowel_letters = [letter for letter in 'Hello' if letter in 'aeiou']
print(vowel_letters)

# Noch mehr Beispiele findest du hier: https://www.w3schools.com/python/python_lists_comprehension.asp


# Ähnlich wie eine Liste kann auch eine Dictionary comprehension erstellt werden:
list_of_words = ['cat', 'window', 'defenestrate']
word_lengths = {word: len(word) for word in list_of_words}
print(word_lengths)