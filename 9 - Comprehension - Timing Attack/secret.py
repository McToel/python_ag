import time
import html
import requests

# generate random german word
def generate_word() -> str:
    word = 'ß'
    while 'ß' in word or 'ü' in word or 'ö' in word or 'ä' in word:
        response = requests.get('https://alex-riedel.de/randV2.php?anz=1')
        word = html.unescape(response.json()[0]).lower()
    return word

password = generate_word()
allowed_chars = 'abcdefghijklmnopqrstuvwxyzäöüß'


# Unless you have a very stable computer,
# you will only be able to crack the length of the password
# if you use this check_password
# def check_password(user, guess):
#     actual = password_database[user]
#     return actual == guess


# Using this check_password, you should be able
# to crack the full password.
def check_password(guess: str) -> bool:
    """Returns True if guess is correct password

    Parameters
    ----------
    guess : str
        password to check

    Returns
    -------
    bool
        True if guess is the correct password
    """    
    if len(guess) != len(password):
        return False

    for i in range(len(password)):
        if guess[i] != password[i]:
            return False
    time.sleep(1e-8)
    return True
