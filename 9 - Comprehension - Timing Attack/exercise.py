# Erstelle eine Liste mit den zahlen 1 bis 10


# Erstelle eine Liste aller geraden Zahlen unter 10


# Erstelle eine Liste mit allen nicht durch 3 teilbaren Zahlen unter 10


# Erstelle eine Liste mit allen Großbuchstaben aus folgendem String: TIPP: https://www.w3schools.com/python/ref_string_isupper.asp
wikepedia_zu_python = '''
Python ist eine universelle, üblicherweise interpretierte, höhere Programmiersprache.
Sie hat den Anspruch, einen gut lesbaren, knappen Programmierstil zu fördern.
So werden beispielsweise Blöcke nicht durch geschweifte Klammern,
sondern durch Einrückungen strukturiert.
'''


# Erstelle eine Dictionary, das dir für jeden Buchstaben seine position im Alphabet anzeigt:
# TIPP: hierbei hilft dir die enumerate() Funktion: https://book.pythontips.com/en/latest/enumerate.html
alphabet = 'abcdefghijklmnopqrstuvwxyz'