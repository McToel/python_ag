import timeit
from secret import check_password, password

print('password:', password, 'len:', len(password))

allowed_chars = 'abcdefghijklmnopqrstuvwxyz'


def time_check_password(guess, trials=1000):
    times = timeit.repeat(
        stmt='check_password(guess)',
        setup=f'guess="{guess}"',
        globals=globals(),
        number=trials,
        repeat=10
    )
    return min(times)


def crack_password_length():
    times = [time_check_password(' ' * i) for i in range(1, 20)]
    return times.index(max(times)) + 1


def crack_password_characters(password_length):
    best_guess = ' ' * password_length
    while not check_password(best_guess):
        for i in range(password_length):
            times = {
                time_check_password(best_guess[:i] + char + best_guess[i + 1:]): char
                for char in allowed_chars
            }

            best_guess = best_guess[:i] + times[max(times)] + best_guess[i + 1:]
            print(best_guess)
            if check_password(best_guess):
                return best_guess
    return best_guess



password_length = crack_password_length()
print(password_length)
input('hit enter to continue...')
print(crack_password_characters(password_length))