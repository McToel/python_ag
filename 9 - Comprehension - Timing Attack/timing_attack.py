import timeit
from secret import check_password, password

# Damit wir ein Gefühl dafür bekommen können, wie gut unser password
# cracker funktioniert, geben wir einfach mal das Passwort aus.
print('password:', password, 'len:', len(password))

allowed_chars = 'abcdefghijklmnopqrstuvwxyz' # Herr Werner hat uns verraten, dass NUR diese Zeichen im passwort vorkommen.


def time_check_password(guess, trials=1000):
    times = timeit.repeat(
        stmt='check_password(guess)',
        setup=f'guess="{guess}"',
        globals=globals(),
        number=trials,
        repeat=10
    )
    return min(times)


def crack_password_length():
    # Diese funktion soll die Länge des Passworts erraten. Vermutlich wird es eine länge zwischen 2 und 20 sein.
    ...


def crack_password_characters(password_length):
    # Diese Funktion soll das Passwort erraten.
    ...



password_length = crack_password_length()
print(password_length)
input('hit enter to continue...')
print(crack_password_characters(password_length))