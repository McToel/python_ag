import random
import time

def visualize(arr, highlight=None):
    """Visualize the array with or without highlight

    Parameters
    ----------
    arr : List[int]
        Array to visualize
    highlight : List[int], optional
        Indicies of arr to highlight, by default None
    """    
    if highlight is None:
        highlight = []

    visulization = ''
    for i, e in enumerate(arr):
        if i in highlight:
            visulization += '> ' + e * '#' + '\n'
        else:
            visulization += '  ' + e * '#' + '\n'
    print(visulization)


def some_sort_algo(arr):
    for i in range(len(arr)):
        for j in range(i, len(arr)):
            visualize(arr, [i, j])
            time.sleep(0.05)
            if arr[i] > arr[j]:
                # swap
                arr[i], arr[j] = arr[j], arr[i]
    return arr

def selection_sort(arr):
    for i in range(len(arr)):
        min_index = arr.index(min(arr[i:]))

        arr[i], arr[min_index] = arr[min_index], arr[i]

        visualize(arr, [i, min_index])
        time.sleep(0.5)
    return arr

# Make data to sort
to_be_sorted = list(range(20))
random.shuffle(to_be_sorted)
print('To be sorted:', to_be_sorted)

def sort(to_sort):
    for i in range(len(to_sort)):
        smallest_element = to_sort[i]
        smallest_index = i
        for index, element in enumerate(to_sort):
            if index <= i:
                continue
            if element < smallest_element:
                smallest_element = element
                smallest_index = index
        to_sort[i], to_sort[smallest_index] = to_sort[smallest_index], to_sort[i]
        visualize(to_sort)
        time.sleep(0.5)

def bogosort(arr):
    tries = 0
    sorted_array = sorted(arr)
    while arr != sorted_array:
        random.shuffle(arr)
        tries += 1
    return tries, arr

print(some_sort_algo(to_be_sorted))

# to_be_sorted.index(min(to_be_sorted))


# Sort the data
# sorted_data = some_sort_algo(to_be_sorted)
# sorted_data = selection_sort(to_be_sorted)

