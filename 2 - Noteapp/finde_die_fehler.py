# Erstelle eine Liste
l = list('foo', 'bar', 'baz')
print(l)


# Gebe das 1. Element der Liste aus
first_element = l[1]
print(first_element)


# Füge der Liste ein Element hinzu
l.add('qux')
print(l)


# Gebe alle Elemente der Liste aus
for element in l
    print(element)


# Lösche das erste Element der Liste
remove l[0]
print(l)