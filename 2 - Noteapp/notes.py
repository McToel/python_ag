notes = [
    'Ich bin eine Notiz',
    'Ich bin noch eine Notiz',
]
options = """
You can add, view, edit, or delete notes.
------------------------------------
a: add a note
t: add todo
d: delete a note
e: edit a note
l: list notes
q: quit
------------------------------------
Enter a letter: """

while True:
    option = input(options)
    if option == 'a':
        note = input('Enter a note: ')
        notes.append(note)

    elif option == 't':
        note = '• ' + input('Enter a todo • ')
        notes.append(note)

    elif option == 'd':
        note = int(input('Enter a note to delete: '))
        del notes[note]

    elif option == 'e':
        note = input('Enter a note to edit: ')
        notes.remove(note)
        note = input('Enter a new note: ')
        notes.append(note)

    elif option == 'l':
        print('\033[37m\nHere are your notes:\x1b[0m')
        for i, note in enumerate(notes):
            print(f'\033[90m{i}:\x1b[0m', f'\033[33m{note}\x1b[0m')
            
    elif option == 'q':
        break