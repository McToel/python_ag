l = ['foo', 'bar', 'baz', 'qux', 'quux', 'corge']

# Lösche ein Element aus der Liste anhand seiner Position
del l[0]


# Iteriere über die Liste und über ihren Index
for i, element in enumerate(l):
    print(i, ':', element)