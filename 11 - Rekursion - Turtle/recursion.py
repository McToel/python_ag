import turtle

def f(length):
    if length > 10:
        f(length/3)
    else:
        turtle.forward(length)
    turtle.left(60)
    if length > 10:
        f(length/3)
    else:
        turtle.forward(length)
    turtle.right(120)
    if length > 10:
        f(length/3)
    else:
        turtle.forward(length)
    turtle.left(60)
    if length > 10:
        f(length/3)
    else:
        turtle.forward(length)

turtle.speed(0)
f(400)

turtle.done()