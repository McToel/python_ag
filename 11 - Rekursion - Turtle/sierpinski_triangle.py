import turtle


def sierpinski_triangle(length=800):
    if length > 15:
        turtle.forward(length)
        turtle.left(120)
        sierpinski_triangle(length/2)
        turtle.forward(length)
        turtle.left(120)
        sierpinski_triangle(length/2)
        turtle.forward(length)
        turtle.left(120)
        sierpinski_triangle(length/2)


# turtle.penup()
# turtle.goto(-800, 200)
# turtle.pendown()
turtle.speed(0)
turtle.width(4)
sierpinski_triangle()

turtle.done()