import turtle

def triangle_shape(length=450):
    if length < 20:
        turtle.forward(length)
    else:
        triangle_shape(length/3)
    turtle.left(60)
    if length < 20:
        turtle.forward(length)
    else:
        triangle_shape(length/3)
    turtle.right(120)
    if length < 20:
        turtle.forward(length)
    else:
        triangle_shape(length/3)
    turtle.left(60)
    if length < 20:
        turtle.forward(length)
    else:
        triangle_shape(length/3)


turtle.penup()
turtle.goto(-800, 200)
turtle.pendown()
turtle.speed(0)
turtle.width(4)

triangle_shape()
turtle.right(160)
triangle_shape()
turtle.right(160)
triangle_shape()
turtle.right(160)
triangle_shape()
turtle.right(160)
triangle_shape()
turtle.right(160)
triangle_shape()
turtle.right(160)
triangle_shape()
turtle.right(160)
triangle_shape()
turtle.right(160)
triangle_shape()
turtle.right(160)

turtle.done()