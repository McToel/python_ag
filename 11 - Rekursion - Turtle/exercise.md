# Turtle
Wir verwenden heute die Turtle um die wildesten Formen zu zeichnen. Die Docs gibt es hier: https://docs.python.org/3/library/turtle.html

## Aufgaben
1. Male ein Quadrat:

![](square.png)

2. Male folgendes Fraktal:

![](triangle.png)

Basis:

![](triangle_base.png)

3. Noch ein Fraktal:

![](squares_fraktal.png)

Dieses Fraktal sieht deutlich komplizierter aus als es eigentlich ist. Die "uhrform" ist kaum noch erkennbar:

![](squares_fraktal_base.png)

4. Hier noch ein letztes Fraktal. Zeichnet man am beseten mit eienr etwas anderen Logik, aber das findest du schon heraus. Das ganze heißt angeblich Sierpinski Triangle.

![](sierpinski_triangle.png)

Die Basisform ist: Surprise!!! Ein Dreick