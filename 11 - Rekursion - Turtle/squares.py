import turtle

def square_shape(length=200):
    if length < 10:
        turtle.forward(length)
    else:
        square_shape(length/3)
    turtle.left(90)

    if length < 10:
        turtle.forward(length)
    else:
        square_shape(length/3)
    turtle.right(90)

    if length < 10:
        turtle.forward(length)
    else:
        square_shape(length/3)
    turtle.right(90)

    if length < 10:
        turtle.forward(length)
    else:
        square_shape(length/3)
    turtle.left(90)

    if length < 10:
        turtle.forward(length)
    else:
        square_shape(length/3)


turtle.speed(0)
turtle.width(5)
square_shape()
turtle.right(90)
square_shape()
turtle.right(90)
square_shape()
turtle.right(90)
square_shape()

turtle.done()