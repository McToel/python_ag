# Gebe den Inhalt der Datei 'secrets.txt' in die Konsole aus


# Überlege die eine Weisheit, und schreibe sie in die
# Datei 'my_secrets.txt'


# Überlege dir noch eine Weisheit, und hänge sie an die
# Datei 'my_secrets.txt' an.


# Überprüfe, ob jetzt tatsächlich beide Weisheiten in der
# Datei 'my_secrets.txt' sind.


# David Kriesel ist Data Scientist. In seiner Freizeit schreib er einen
# sehr emfpehlenswerten Bolg (https://www.dkriesel.com/) über seine Hobbyprojekte.
# Unter anderem liefert er immer aktuelle Plots über die Corona-Krise (https://www.dkriesel.com/corona).
# Den Plot zu Deutschland findet man unter diesem Link hier:
# https://www.dkriesel.com/_media/coronaplot-germany.png
# Speichere diesen Plot unter dem namen 'covid-deutschland.png'.
# TIPP: Hier wird erklärt, wie man ein Bild herunterladen und speichern kann.
# https://stackoverflow.com/questions/13137817/how-to-download-image-using-requests/21595698#21595698


# In Tübingen gibt es immer viel zu Arbeiten. Daher steht auf der
# Hompage (https://www.tuebingen.de/) von Tübingen immer,
# wie viele offene Stellen die Stadtverwaltung hat.
# Analysiere die Website und versuche eine Request zu finden, die dir die Anzahl
# der offenen Stellen gibt.
# Öffne dafür mit F12 die Developer Tools in deinem Browser.


# Schreibe ein Script stellencount.py, das jede Stunde die Anzahl der offenen Stellen aus der
# Tübingen Website ausliest und in einer Datei schreibt.


# Das B12 hat auf ihrer Homepage (https://b12-tuebingen.de/) eine Anzeige,
# wie viele Plätze noch frei sind.
# Dieser Link hier gibt dir ein HTML-Element, das die Anzahl der freien Plätze
# anzeigt: https://111.webclimber.de/de/trafficlight?key=184xNhv6RRU7H2gVg8QFyHCYxym8DKve
# Versuche die Anzahl der freien Plätze zu extrahieren und speichere diese auch stündlich in einer Datei


# Bei den beiden Programmen wird nun noch nicht die Uhrzeit gespeichert. Immer wenn wir Daten sammeln,
# ist der Zeitpunkt der Speicherung besonders wichtig. Füge jetzt noch die Zeit bei den Daten hinzu.
# Dokumentation datetime Modul: https://docs.python.org/3/library/datetime.html
# Ich würde hier an dieser Stelle aber einfach mal im Internet nach einer Antwort suchen wie z.B.
# python get current date an time as string

