# Virtualenviroments
Oft brauchen wir in Python externe Packages. Das ist Code von anderen, den wir mitverwenden können, um nicht alles neu Schreiben zu müssen. Wir verwenden dafür das command-line-tool `pip` (**p**ackage **i**nstaller for **P**ython). Damit man auf seinem Computer nicht der überblick verliert, sollte man für jedes Projekt eine eigene Umgebung (Enviroment) erstellen, indie nur die Packages für dieses Projekt installiert werden. Das geht wie folgt:
```bash
# Virtualenviroment erstellen
python3 -m venv pfad_für_das_neue_enviroment

# Virtualenviroment aktivieren
source pfad_für_das_neue_enviroment/bin/activate

# Einzelnes package installieren
pip install package_name

# Alle packages aus einer Datei installieren
pip install -r requirements.txt

# Virtualenviroment deaktivieren (braucht man fast nie)
deactivate
```

## Aufgaben
1. Erstelle ein neues virtualenviroment namens `env` im Ordner dev/python_ag/
2. Aktiviere das Enviroment in vscode. Hier solltest du eine Benachrichtigung für erhalten haben
3. Aktiviere das Enviroment in der commandline
4. Installiere das package `matplotlib`
5. Installiere requirements.txt