# https://www.w3schools.com/python/ref_func_open.asp

f = open('hello_world.txt', 'w')
f.write('Hello World!\n')
f.close()

# Schöner und mit append:
with open('hello_world.txt', 'a') as f:
    f.write('Hello Again!')

# Lese Datei ein:
with open('hello_world.txt', 'r') as f:
    print(f.read())