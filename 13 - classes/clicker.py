import random

class Enemy:
    def __init__(self, name, hp, damage):
        self.name = name
        self.hp = hp
        self.damage = damage
        self.coins = random.randint(1, 10)

    def is_alive(self):
        return self.hp > 0

    def take_damage(self, damage):
        self.hp -= damage
        print(f"{self.name} took {damage} damage. It now has {self.hp} health.")
        if not self.is_alive():
            self.die()

    def die(self):
        print(f"{self.name} is dead.")


class Troll(Enemy):
    def __init__(self):
        super().__init__('Troll', hp=30, damage=5)


class Vampire(Enemy):
    def __init__(self):
        super().__init__('Vampire', hp=50, damage=10)


enemies = [Troll, Vampire]
coins = 0
damage = 1

while True:
    enemy = random.choice(enemies)()
    while enemy.is_alive():
        option = input("Press Enter to attack!")
        if option == 's':
            print('This is a shop!', f"You have {coins} coins.")
            option = input("What do you want to buy? (1) damage (15 coins)")
            if option == '1':
                if coins >= 15:
                    coins -= 15
                    damage += 1
                    print(f"You now have {coins} coins.")
                else:
                    print("You don't have enough coins!")

        enemy.take_damage(damage)
    coins += enemy.coins
    print(f"You have {coins} coins.")
