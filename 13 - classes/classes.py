# Definiere die Klasse MyClass
class MyClass:
    """A simple example class"""
    # Klassen-Attribut
    i = 12345

    # Klassen-Methode (sieht so aus wie eine Funktion,
    # und ist auch eigentlich eine Funktion, wird aber
    # Methode genannt). Wichtig ist, dass das self-Argument
    # nicht vergessen wird.
    def f(self):
        return 'hello world'

# Erstelle eine Instanz von MyClass
my_class = MyClass()

# Durch die Instanz können wir die Methoden und variablen
# der Klasse aufrufen
print(my_class.i)
print(my_class.f())

# Die Klassenvariablen können auch direkt aufgerufen werden
print(MyClass.i)


# Ein weiteres Beispiel: Eine Klasse zum rechnen mit complexen
# Zahlen
class Complex:
    # Die __init__-Methode wird aufgerufen, wenn eine neue
    # Instanz erstellt (initialisiert) wird.
    def __init__(self, realpart, imagpart):
        self.r = realpart
        self.i = imagpart

    def __str__(self) -> str:
        return f'{self.r} + {self.i}i'

    def __add__(self, other):
        return Complex(self.r + other.r, self.i + other.i)

# Die Werte 3.0 und -4.5 werden an die __init__-Methode übergeben
x = Complex(3.0, -4.5)
print('x =', x)

y = Complex(-1.0, 3.5)
print('y =', y)

print('x + y =', x + y)