# Die Klasse Vehicle dient als Basisklasse für weitere,
# speziefische Fahrzeuge. Diese Erben die Methoden und
# Attribute der Basisklasse.
class Vehicle:
    def __init__(self, max_speed, seats, wheels):
        self.max_speed = max_speed
        self.seats = seats
        self.wheels = wheels

    def passenger_km_per_hour(self):
        return self.max_speed * self.seats


# Die Klasse Bicycle erbt nun alle Methoden und Attribute
# der Basisklasse Vehicle.
class Bicycle(Vehicle):
    # Wir überschreiben die __init__-Methode der Basisklasse
    # da wir wissen, das ein Fahrrad immer 2 Räder hat. und 
    # meistens auch nur einen Sitzplatz. (Ausnamen: Tanden
    # oder Liegefahrrad mit 3 Rädern)
    def __init__(self, max_speed, seats=1, wheels=2):
        # Mit super() kann die init-Methode der Basisklasse
        # aufgerufen werden.
        super().__init__(max_speed, seats, wheels)


class ICE(Vehicle):
    def __init__(self, baureihe):
        # Beim ICE gehen wir noch einen Schritt weiter, da
        # es überhaupt nur ca. 15 verschiedene Bauriehen gibt.
        # Jede Baureihe hat eine Maximalgeschwindigkeit und eine
        # fixe Anzahl an Sitzplätzen, Wagen und Räder.
        if baureihe == 401:
            self.carriages = 14
            super().__init__(max_speed=280, seats=703, wheels=(self.carriages + 2) * 8)
        elif baureihe == 402:
            self.carriages = 6
            super().__init__(max_speed=280, seats=381, wheels=(self.carriages + 2) * 8)


# Aufgaben:
# Erstelle ein instance von Bicycle, welches so schnell fährt,
# wie du normalerweise ein Fahrrad fährst.
...


# Erstelle ein instance von ICE, mit der Baureihe 402.
...


# Erstelle eine Klasse Car, die von Vehicle erbt.
...


# Ein ICE war laut DB AG 2019 zu durchschnittlich 59%
# ausgelastet. Ein PKW wird mit durchschnittlich 1.5
# Personen besetzt.
# Wie viele Autos braucht man durchschnittlich auf
# einer Autobahn (130km/h) um einen ICE der Bauhreihe
# 402 auf einer Hochgeschwindigkeitsstrecke (280km/h)
# zu ersetzen?
# Benutze dafür die Methode passenger_km_per_hour.
# Es könnte sich auch lohen, die Klassen so umzubauhen,
# dass diese auch die durchschnittliche Besetzung eines
# Fahrzeuges speichern.
...