import random

class Enemy:
    def __init__(self, name, hp, damage, coins):
        self.name = name
        self.hp = hp
        self.damage = damage
        self.coins = coins

    def is_alive(self):
        """Lebt der Gegner noch?

        Returns
        -------
        bool
            Ob der Gegner noch lebt oder nicht.
        """
        return self.hp > 0

    def take_damage(self, damage):
        """Nehme schaden

        Parameters
        ----------
        damage : int
            Wie viel Schaden soll der Gegner erleiden.
        """        
        self.hp -= damage
        print(f"{self.name} took {damage} damage. It now has {self.hp} health.")
        if not self.is_alive():
            self.die()

    def die(self):
        """Gegner stirbt"""
        print(f"{self.name} is dead.")


# Aufgabe: Erstelle mindestens 3 verschiedene Gegner Klassen, die
# jeweils einen Namen, Lebenspunkte, Schaden und Geld haben.

# Baue daraus ein Spiel. Der Spieler kann dem Gegner schaden gegeben
# indem er Enter drückt. Wenn der Gegner stirbt, bekommt der Spieler
# das Geld des Gegners und ein neuer Gegner erscheint.

# Zusätzlich kannst du einen Shop erstellen, in man mit den verdienten
# coins etwas kaufen kann. Z.B. mehr Schaden oder so.
